using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace APIEstudiantes.Controllers;

[ApiController]
[Authorize]
[Route("api/estudiantes")]
public class EstudiantesController : ControllerBase
{
    public static List<Estudiante> estudiantes = new List<Estudiante>() { };

    private readonly ILogger<EstudiantesController> _logger;

    public EstudiantesController(ILogger<EstudiantesController> logger)
    {
        _logger = logger;
    }

    [HttpGet(Name = "GetEstudiantes")]
    [Authorize("read:estudiantes")]
    public IEnumerable<Estudiante> Get()
    {
        return estudiantes;
    }

    [HttpGet("{id}")]
    [Authorize("read:estudiantes")]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public ActionResult<Estudiante> Get(int id)
    {
        int estudianteIndex = estudiantes.FindIndex(r => r.Id == id);
        if (estudianteIndex < 0)
        {
            return NotFound();
        }
        return estudiantes[estudianteIndex];
    }


    [HttpPost(Name = "PostEstudiantes")]
    [Authorize("write:estudiantes")]
    public void Post(Estudiante estudiante)
    {
        estudiante.Id = 1;
        if (estudiantes.Count > 0)
            estudiante.Id = estudiantes.Max(r => r.Id) + 1;
        estudiantes.Add(estudiante);
    }

    [HttpPut("{id}",Name = "PutEstudiantes")]
    [Authorize("write:estudiantes")]
    public ActionResult Put(int id, Estudiante estudiante)
    {
        int estudianteIndex = estudiantes.FindIndex(r => r.Id == id);
        if (estudianteIndex < 0)
        {
            return NotFound();
        }
        estudiantes[estudianteIndex] = estudiante;
        return new EmptyResult();
    }
    [HttpDelete("{id}",Name = "DeleteEstudiantes")]
    [Authorize("write:estudiantes")]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public ActionResult Delete(int id)
    {
        int estudianteIndex = estudiantes.FindIndex(r => r.Id == id);
        if (estudianteIndex < 0)
        {
            return NotFound();
        }
        estudiantes.RemoveAt(estudianteIndex);
        return new EmptyResult();
    }
}